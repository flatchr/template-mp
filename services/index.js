const axios = require('axios');
const dotenv = require('dotenv');

dotenv.config();

exports.sendInvitation = async (payload) => {
  try {
    const {
      token,
      email,
      firstName,
      lastName,
      locale,
    } = payload;

    const baseURL = process.env.API_ENDPOINT;
    axios.defaults.baseURL = baseURL;
    axios.defaults.headers.post['Content-Type'] = 'application/json';
    axios.defaults.headers.post['Token'] = token;

    const {
      data,
    } = await axios.post('/send-invitation', {
      email,
      firstName,
      lastName,
      locale
    });
    return data;
  } catch (error) {
    if (error.response) {
      throw new Error(error.response.message);
    }
    throw new Error(error.message || error);
  }
};

exports.saveDatas = async (payload) => {
  try {
    const {
      companyId,
      email
    } = payload;

    const baseURL = process.env.FLATCHR_API_ENDPOINT;
    axios.defaults.baseURL = baseURL;
    axios.defaults.headers.post['Content-Type'] = 'application/json';

    // Required payload to call "/mp/incoming" api endpoint
    const params = {
      app_name: 'YOUR_APP_NAME',
      reference: null, // NULL IF NO APPLICANT ID
      type: 'applicants',
      company_id: companyId, // ID PROVIDED BY FLATCHR
      candidate_email: email, // CANDIDATE EMAIL
      value: {
        url: "https://beeceptor.com/console/flatchr", // REPLACE IT BY YOUR URL
        external: true, // IF LINK OPEN A NEW PAGE
        status: 1, // STATUS 1 = ACTIVE, STATUS 0 = INACTIVE
        position: 'info', // DO NOT TOUCH THIS
      },
    };

    const savedData = await axios.post('/mp/incoming', params);

    if (savedData) {
      return {
        success: true,
      };
    }
  } catch (error) {
    if (error.response) {
      throw new Error(error.response.message);
    }
    throw new Error(error.message || error);
  }
};