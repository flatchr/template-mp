const express = require('express');

const {
  sendInvitation,
  saveDatas
} = require('../services');

const router = express.Router();

router.get('/', function (req, res) {
  return res.status(200).end();
});

// 1. HANDLE FLATCHR REQUEST

// router.post('/[YOUR APP NAME]/[EXEMPLE]', {}, async (req, res) => {
//   try {
//     Do your logic here
//   } catch (err) {
//     return res.status(500).send({
//       message: 'Error',
//       err,
//     });
//   }
// });

router.post('/app-exemple/exemple', async (req, res) => {
  try {
    const {
      success,
    } = await sendInvitation(req.body);

    if (success) {
      return res.status(200).send({
        message: 'Invitation send. Watch your result on: https://beeceptor.com/console/flatchr',
      });
    }
  } catch (err) {
    return res.status(500).send({
      message: 'Error',
      err,
    });
  }
});

// 2. POST ADDITIONNAL DATA TO FLATCHR
/**
 * Callback to Flatchr
 */
router.post('/app-exemple/exemple-hook', async (req, res) => {
  try {
    const {
      success,
    } = await saveDatas(req.body);

    if (success) {
      return res.status(200).send({
        message: 'Datas saved. Watch your result on: https://beeceptor.com/console/flatchr',
      });
    }
  } catch (err) {
    return res.status(500).send({
      message: 'Error',
      err,
    });
  }
});

module.exports = router;
